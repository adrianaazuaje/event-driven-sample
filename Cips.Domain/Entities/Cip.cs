using System;

namespace Cips.Domain.Entities
{
    public class Cip
    {
        public Guid CipCode { get; set; }
        public decimal Total  { get; set; }
        public string CurrencyCode { get; set; } 
        public string UserEmail { get; set; }
    }
}