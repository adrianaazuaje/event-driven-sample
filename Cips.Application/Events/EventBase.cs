using System;
using System.Text.Json;

namespace Cips.Application.Events
{
    public abstract class EventBase
    {
        protected object eventData;

        protected EventBase(object eventData)
        {
            this.eventData = eventData;
        }
        
        public abstract string GetEventId();

        public string Serialize()
        {
            return JsonSerializer.Serialize(eventData);
        }
        
    }
}